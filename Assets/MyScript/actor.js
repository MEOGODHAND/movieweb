const apikey = '0ed4397ec3558e87785cdb45c209ca56';
const api = 'https://api.themoviedb.org/3/';
const pathImage = 'https://image.tmdb.org/t/p/w500';
var curPage = 1;
var tabID = "person";
$(document).ready(function () {
    getData(curPage, tabID, 0).then(function (result) {
        fillActors(result);
        //pagination(result.total_pages);
    });
    $('#search').click(async function (e) {
        e.preventDefault();
        $('#mainContent .row').append(animationLoading);
        //$('#pagination').children().empty();
        const strSearch = $('form input').val();
        const reqStr = `${api}search/person?api_key=${apikey}&query=${strSearch}&page=1`;
        const reponse = await fetch(reqStr);
        const rs = await reponse.json();
        fillActors(rs);
        pagination(rs.total_pages);
    })
})
async function getData(pageIndex, tabID, def = 0) {
    $('#mainContent .row').append(animationLoading);
    const strSearch = $('form input').val();
    let topRating;
    if (def == 0) {
        topRating = `${api}${tabID}/popular?api_key=${apikey}&&language=en-US&page=${pageIndex}`;
    }
    else {
        topRating = `${api}search/person?api_key=${apikey}&query=${strSearch}&page=${pageIndex}`;
    }
    console.log(topRating);
    const response = await fetch(topRating);
    const dataOfTab = await response.json();
    return dataOfTab;
}
function pagination(totalpages) {
    $('#fixbug1h').remove();
    $('#mainContent .row').after(`<nav aria-label="Page navigation" id="fixbug1h">
                        <ul class="pagination justify-content-center" id="pagination"></ul>
                    </nav>
`);
    window.pagObj = $('#pagination').twbsPagination({
        totalPages: totalpages,
        visiblePages: 9,
        onPageClick: function (event, page = 1) {
            console.info(page + ' (from options)');
        }
    }).on('page', function (event, page = 1) {
        console.info(page + ' (from event listening)');
        getData(page, tabID,1).then(function (result) {
                fillActors(result);
        });
    });
}
function fillActors(receivedData) {
    $('#mainContent .row').empty();
    for (const item of receivedData.results) {
        if (item.profile_path == null) {
            $('#mainContent .row').append(`<div class="col-sm-4 d-flex pb-3" onclick="showModalActor(${item.id})">
        <div class="card h-100">
        <img src="./Assets/Images/avatar.png" class="card-img-top" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">${item.name}</h5>
            <p class="card-text">Image not found </p>
        </div>
    </div>
</div>`)
        }
        else {

            $('#mainContent .row').append(`<div class="col-sm-4 d-flex pb-3" onclick="showModalActor(${item.id})">
        <div class="card h-100">
        <img src="${pathImage}${item.profile_path}" class="card-img-top" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">${item.name}</h5>
        </div>
    </div>
</div>`)
        }
    }
}
function animationLoading() {
    return `<div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-light" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-dark" role="status">
  <span class="sr-only">Loading...</span>
</div>`;
}
function showModalActor(idAct) {
    $('#myModal').modal('hide');
    getDetailActor(idAct).then(function (result) {
        fillActorModal(result);
        //$('#myModal').modal('toggle');
    })
}
async function getDetailActor(idActor) {
    //$('#mainContent .row').append(animationLoading);
    const reqStr = `${api}${tabID}/${idActor}?api_key=${apikey}&append_to_response=movie_credits`;
    console.log(reqStr);
    const response = await fetch(reqStr);
    const dataOfTab = await response.json();
    console.log(dataOfTab);
    return dataOfTab;
}
function fillActorModal(dataOfTab) {
    let GENDER = ["Female", "Male"];
    $('#ModalTitle').html(dataOfTab.name);
    $('#imgModal').attr("src", `${pathImage}${dataOfTab.profile_path}`);
    $('#myModal .col-sm-8').empty();
    $('#bonusInfo').empty();
    $('#myModal .col-sm-8').append(`<table style="width:100%">
                                <tr>
                                    <th>Biography</th>
                                    <td id="biography">${dataOfTab.biography}</td>
                                </tr>
                                <tr>
                                    <th>Birthday </th>
                                    <td id="birthday">${dataOfTab.birthday}</td>
                                </tr>
                                <tr>
                                    <th>Place of birth </th>
                                    <td id="place_of_birth">${dataOfTab.place_of_birth}</td>
                                </tr>
                                <tr>
                                    <th>Gender </th>
                                    <td id="gender">${GENDER[parseInt(dataOfTab.gender) - 1]}</td>
                                </tr>
                                <tr>
                                    <th>Deathday </th>
                                    <td id="deathday">${dataOfTab.deathday}</td>
                                </tr>
                            </table>`);
    $('#bonusInfo').empty();
    $('#bonusInfo').append(`
                    <h1>His/Her Films</h1>
                    <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8" id="films"></div>
                    <div class="col-2"></div>
                    </div>
`)
    $('#films').append(`<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
   
  </ul>
  <div class="carousel-inner">

  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>`);
    let i = 0;
    for (const item of dataOfTab.movie_credits.cast) {
        let srcImg;
        if (item.backdrop_path == null) {
            srcImg = "./Assets/Images/White_Poster2.jpg";
        }
        else {
            srcImg = `${pathImage}${item.backdrop_path}`;
        }
        if (i == 0) {
            $('.carousel-indicators').append(`<li data-target="#demo" data-slide-to="${i}" class="active"></li>`);
            $('.carousel-inner').append(`  <div class="carousel-item active" onclick="showModalMovie(${item.id})">
            <img src="${srcImg}" alt="ABC" width="1100" height="500">
                <div class="carousel-caption">
                    <h3>${item.title}</h3>
                    <p>${item.release_date}</p>
                </div>   
    </div>
            `);
        }
        else {
            $('.carousel-indicators').append(`<li data-target="#demo" data-slide-to="${i}"></li>`);
            $('.carousel-inner').append(`  <div class="carousel-item" onclick="showModalMovie(${item.id})">
            <img src="${srcImg}" alt="Los Angeles" width="1100" height="500">
                <div class="carousel-caption">
                    <h3>${item.title}</h3>
                    <p>${item.release_date}</p>
                </div>   
    </div>
            `);
        }


        i++;
    }
    $('#myModal').modal('show');
    $("#biography").shorten({
        "showChars": 150,
        "moreText": "See More",
        "lessText": "Less",
    });
}
function closeModal() {
    $('#myModal').modal('hide');
}
function fillMovieModal(dataOfTab) {
    $('#ModalTitle').html(dataOfTab.title);
    if (dataOfTab.poster_path != null) {
        $('#imgModal').attr("src", `${pathImage}${dataOfTab.poster_path}`);
    }
    else {
        $('#imgModal').attr("src", "./Assets/Images/White_Poster.gif");
    }
    $('#myModal .col-sm-8').empty();
    $('#bonusInfo').empty();
    $('#myModal .col-sm-8').append(`<table style="width:100%">
                                <tr>
                                    <th>Overview   </th>
                                    <td id="overviewModal">1234dadaad.....</td>
                                </tr>
                                <tr>
                                    <th>Genres </th>
                                    <td id="genresModal">55577854</td>
                                </tr>
                                <tr>
                                    <th>Casts </th>
                                    <td id="castsModal" style="display:flex">
                                        <a class="nav-link" id="19429" style="color:darkgray" onclick="rrrr(this.id)">BruceLee</a>
                                        <a class="nav-link" id="19429" onclick="searchPeople(this.id)">BruceLee</a>
                                        <a class="nav-link" id="19429" onclick="searchPeople(this.id)">BruceLee</a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Country </th>
                                    <td id="countryModal">USA</td>
                                </tr>
                                <tr>
                                    <th>Publish year </th>
                                    <td id="releaseDateModal">No imformation</td>
                                </tr>
                                <tr>
                                    <th>Director </th>
                                    <td id="directorModal">No imformation</td>
                                </tr>
                            </table>`);
    $('#overviewModal').html(dataOfTab.overview);
    let genres = '';
    for (const item of dataOfTab.genres) {
        genres = genres + item.name + ' ,  ';
    }
    $('#genresModal').html(genres);
    let countries = '';
    for (const item of dataOfTab.production_countries) {
        countries = countries + item.name + ' ,  ';
    }
    $('#castsModal').empty();
    let i = 0;
    for (const item of dataOfTab.credits.cast) {
        $('#castsModal').append(`<a class="nav-link" data-toggle="tooltip" title="please click to view detail!" style="color:darkgray" id="${item.id}" onclick="showModalActor(this.id)">${item.name}</a>`);

        i = i + 1;
        if (i >= 4) {
            $('#castsModal').append("...");
            break;
        }
    }
    i = 0;

    $('#bonusInfo').empty();
    $('#bonusInfo').append(`
                    <h1>Reviews</h1>
                    <dl class="dl-horizontal" id="reviewFilm">
                    </dl>
`)
    for (const item of dataOfTab.reviews.results) {
        $('#reviewFilm').append(`<dt>${item.author}</dt>
                        <dd>${item.content}</dd>
`);
        i = i + 1;
        if (i >= 4) {
            $('#bonusInfo').append(`<dt>...</dt>
                        <dd>...................</dd>
`)
            break;
        }
    }
    let directors = "";
    for (const item of dataOfTab.credits.crew) {
        if (item.department == "Directing") {
            directors = directors + item.name + ' ,  ';
        }
    }
    $('#countryModal').html(countries);
    $('#directorModal').html(directors);
    $('#releaseDateModal').html(dataOfTab.release_date);
    $('#myModal').modal('show');
    $("dd").shorten({
        "showChars": 200,
        "moreText": "See More",
        "lessText": "Less",
    });
    $('[data-toggle="tooltip"]').tooltip();
}
async function getDetailMovie(id) {
    //$('#mainContent .row').append(animationLoading);
    //https://api.themoviedb.org/3/movie/475557/credits?api_key=0ed4397ec3558e87785cdb45c209ca56
    const reqStr = `${api}movie/${id}?api_key=${apikey}&append_to_response=videos,credits,reviews`;
    console.log(reqStr);
    const response = await fetch(reqStr);
    const dataOfTab = await response.json();
    console.log(dataOfTab);
    return dataOfTab;
}
function showModalMovie(idMovie) {
    $('#myModal').modal('hide');
    getDetailMovie(idMovie).then(function (result) {
        fillMovieModal(result);
        //$('#myModal').modal('toggle');
    })
}